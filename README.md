# FER (Filter-Extract-Replace)

FER is meant as a simple and efficient regex utility combining some of the basic functionalities of sed and grep.

It uses rust's [Regex crate](https://docs.rs/regex/1.4.2/regex/index.html#syntax)'s syntax (on which it is based), as well as its [replacement syntax](https://docs.rs/regex/1.4.2/regex/struct.Regex.html#replacement-string-syntax).

## Usage

```    
fer [FLAGS] [OPTIONS] <EXPR1> [FILENAME]

FLAGS:
    -e, --extract          Return capture groups
    -f, --filter           Filter lines containing expression
    -o, --only-first       Print only first match per line when extracting.
    -h, --help             Prints help information
    -n, --line-numbers     Print line numbers when filtering
    -m, --only-matching    Print only matching string when filtering. When replacing, only prints lines with
                           replacement.
    -V, --version          Prints version information

OPTIONS:
    -r, --replace <EXPR2>    Replace expression

ARGS:
    <EXPR1>       Search expression
    <FILENAME>    Input file     
```
