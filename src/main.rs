use clap::{self, App, Arg, Error, ErrorKind};
use colored::*;
use regex::Regex;
use std::fs::File;
use std::io::{self, BufRead, BufReader};

const NAME: &str = clap::crate_name!();
const VERSION: &str = clap::crate_version!();
const AUTHOR: &str = clap::crate_authors!();

/// Parses the commandline arguments
fn parse_args() -> clap::ArgMatches<'static> {
    let matches = App::new(NAME)
        .version(VERSION)
        .author(AUTHOR)
        .about("Filter, extract and replace from files or stdin.")
        .arg(
            Arg::with_name("filter")
                .short("f")
                .long("filter")
                .help("Filter lines containing expression"),
        )
        .arg( 
            Arg::with_name("extract")
                .short("e")
                .long("extract")
                .help("Return capture groups"),
        )
        .arg(
            Arg::with_name("replace")
                .short("r")
                .long("replace")
                .value_name("EXPR2")
                .takes_value(true)
                .help("Replace expression"),
        )
        .arg(
            Arg::with_name("linenumbers")
                .short("n")
                .long("line-numbers")
                .help("Print line numbers when filtering"),
        )
        .arg(
            Arg::with_name("onlymatching")
                .short("m")
                .long("only-matching")
                .help("Print only matching string when filtering. When replacing, only prints lines with replacement."),
        )
        .arg(
            Arg::with_name("firstmatch")
                .short("o")
                .long("only-first")
                .help("Print only first match per line when extracting."),
        )
        .arg(
            Arg::with_name("EXPR1")
                .help("Search expression")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("FILENAME")
                .help("Input file")
                .required(false)
                .index(2),
        )
        .get_matches();

        return matches;
}

/// Check whether the arguments are compatible
fn check_args(filter: bool, extract: bool, replace: bool) {
    if (filter as usize + replace as usize + extract as usize) > 1 {
        Error::with_description(
            "Flags -f -e -r are mutually exclusive.",
            ErrorKind::ArgumentConflict,
        )
        .exit();
    } else if (filter as usize + replace as usize + extract as usize) == 0 {
        Error::with_description(
            "At least one of -f -e -r is required.",
            ErrorKind::MissingRequiredArgument,
        )
        .exit();
    }
}

fn main() {
    
    let matches = parse_args();

    let filter = matches.is_present("filter");
    let extract = matches.is_present("extract");
    let replace = matches.is_present("replace");

    // Should we display line numbers
    let linenumbers = matches.is_present("linenumbers");
    // Should we only print matching lines
    let matchingonly = matches.is_present("onlymatching");
    //Should we only print the first matching line
    let firstmatch = matches.is_present("firstmatch");

    check_args(filter, extract, replace);

    // Open file if present
    let lines: Vec<io::Result<String>> = if matches.is_present("FILENAME") {
        let file = if let Ok(file) = File::open(matches.value_of("FILENAME").unwrap()) {
            file
        } else {
            Error::with_description("Could not open file.", ErrorKind::InvalidValue).exit()
        };
        BufReader::new(file).lines().collect()
    // Otherwise read from stdin
    } else {
        let stdin = io::stdin();
        stdin.lock().lines().collect()
    };


    // Compile search expression
    let search_expression = if let Ok(regex) = Regex::new(matches.value_of("EXPR1").unwrap()) {
        regex
    } else {
        Error::with_description("Invalid expression.", ErrorKind::InvalidValue).exit()
    };


    // TODO Move statement elsewhere 
    let replace_expression = matches.value_of("replace");

    // Line counter
    let mut i = 0;
    for line in lines {

        // Check if can read line
        let line = if let Ok(line) = line {
            line
        } else {
            Error::with_description("Error reading input.", ErrorKind::Io).exit()
        };

        i += 1;

        // Add linenumber to prefix
        let prefix = if linenumbers {
            format!("{}: ", i.to_string().red())
        } else {
            "".to_string()
        };

        // Is line matching?
        let maching_line = search_expression.is_match(&line);

        
        if maching_line {

        } else if 





        if maching_line || (replace && !matchingonly) {
            let prefix = if linenumbers {
                format!("{}: ", i.to_string().red())
            } else {
                "".to_string()
            };

            let output = if filter {
                if matchingonly {
                    let mut output = String::new();
                    for c in search_expression.captures_iter(&line) {
                        output = format!("{}{} ", output, &c[0]);
                    }
                    output.pop();
                    output
                } else {
                    line
                }
            } else if extract {
                let mut output = String::new();
                for c in search_expression.captures_iter(&line) {
                    let mut c = c.iter();
                    c.next();
                    for group in c {
                        if group.is_none() {
                            continue;
                        }
                        output = format!("{}{} ", output, &group.unwrap().as_str());
                    }
                    if firstmatch {
                        break;
                    }
                }
                output.pop();
                output
            } else if replace & maching_line {
                search_expression
                    .replace_all(&line, replace_expression.unwrap())
                    .to_string()
            } else {
                // Error::with_description(
                //     "At least one of -f -e -r is required.",
                //     ErrorKind::MissingRequiredArgument,
                // )
                // .exit();
            };

            println!("{}{}", prefix, output);
        }
    }
}
